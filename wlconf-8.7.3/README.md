#Hancock Wifi - wl1837

Binary is matching following commands

##Howto 
```bash
./configure-device.sh

Please provide the following information.

Are you using a TI module? [y/n] : y
What is the chip flavor? [1801/1805/1807/1831/1835/1837 or 0 for unknown] : 1837
Should Japanese standards be applied? [y/n] : n
How many 2.4GHz antennas are fitted? [1/2] : 2
How many 5GHz antennas are fitted (using 2 antennas requires a proper switch)? [0/1/2] : 2

The device has been successfully configured.
TI Module: y
Chip Flavor: 1837
Number of 2.4GHz Antennas Fitted: 2
Number of 5GHz Antennas Fitted: 2
Diversity Support: y
SISO40 Support: y
Japanese Standards Applied: n
Class 2 Permissive Change (C2PC) Applied: n

```
##Log
```bash
md5sum
7d276eb47288e94712eccf9dd43204dc  wl18xx-conf.bin
```
